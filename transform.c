#include <stdio.h>
#include <dlfcn.h>

/** Calls function transform from any given .so on string "Hello World!".
 *
 *  n. arg: .so with function char *(*transform)(const char *, char *)
 *  -> prints transformed "Hello World!" for each .so
 *
 *  prepared .so:
 *  	upper.so -> calls toupper()
 *  	lower.so -> calls tolower()
 *  	censor.so -> substitutes each 'o' for 'X'
 *  	error.so -> ends with error because theres no transform:)
 **/


int main(int argc, char *argv[])
{	
	for (int i = 1; i < argc; i++) {
		void *lib = dlopen(argv[i], RTLD_NOW);

		if (lib == NULL) {
			puts(dlerror());
			return 1;
		}

		char *(*f)(const char *, char *) = dlsym(lib, "transform");
		
		char *error = dlerror();
		if (error != NULL) {
			fprintf(stderr, "%s\n", error);
			dlclose(lib);
			return 1;
		}

		char mem[100];
		printf("%s\n", f("Hello, World!", mem));

		dlclose(lib);
	}

	return 0;
}
