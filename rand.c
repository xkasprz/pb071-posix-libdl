#include <stdlib.h>
#include <stdio.h>
#include <time.h>

char str[100] = {'\0'}; // shhh:)

// seeded only by seconds so its not so random
// /dev/urandom TODO or is there better way?

char *make_sound() // ./make_sound ./rand.so
{
	srand(time(NULL));
	sprintf(str, "%d", rand());
	return str;
}

void any() // ./call_func ./rand.so any
{
	srand(time(NULL));
	printf("%ld\n", rand());
}

void dice() // ./call_func ./rand.so dice
{
	srand(time(NULL));
	printf("%ld\n", (rand() % 6) + 1);
}
