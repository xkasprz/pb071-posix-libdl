#include <ctype.h>

char *transform(const char *from, char *to) // ./transform ./lower.so
{
	int i;

	for (i = 0; from[i] != 0; i++) {
		to[i] = tolower(from[i]);
	}

	to[i] = '\0';

	return to;
}
