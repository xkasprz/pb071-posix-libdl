all: shareds programs

programs: call_func cos make_sound transform void

shareds: cat.so censor.so duck.so error.so lower.so upper.so rand.so void-call.so

call_func: call_func.c
cos: cos.c
make_sound: make_sound.c
transform: transform.c
void: void.c

cat.lo: cat.c
	cc -fPIC $< -c -o $@
censor.lo: censor.c
	cc -fPIC $< -c -o $@
duck.lo: duck.c
	cc -fPIC $< -c -o $@
error.lo: error.c
	cc -fPIC $< -c -o $@
lower.lo: lower.c
	cc -fPIC $< -c -o $@
upper.lo: upper.c
	cc -fPIC $< -c -o $@
rand.lo: rand.c
	cc -fPIC $< -c -o $@
void-call.lo: void-call.c
	cc -fPIC $< -c -o $@

%.so: %.lo
	cc -shared $< -o $@

clean:
	-rm *.lo *.o 

test:
	@echo "------------------SOUNDS:"
	./make_sound ./cat.so
	./make_sound ./duck.so
	./make_sound ./rand.so
	@echo "------------------CALLS:"
	./call_func ./cat.so meow
	./call_func ./cat.so purr
	./call_func ./duck.so quack
	./call_func ./rand.so dice
	./call_func ./rand.so any
	@echo "------------------COS:"
	-./cos # might not work, needs to change path
	@echo "------------------TRANSFORM:"
	./transform ./upper.so
	./transform ./lower.so
	./transform ./censor.so
	-./transform ./error.so # should end with error
	@echo "------------------SANITY CHECK:"
	./void ./void-call.so

.PHONY: all clean programs shareds test
