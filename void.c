#include <stdio.h>
#include <dlfcn.h>

/** This is just sanity check, if it starts failing somewhere:)
 *
 *  1. arg: void-call.so
 *  -> nothing (it just shouldn't fail:)
 *
 *  prepared .so:
 *  	void-call.so
 **/

int main(int argc, char *argv[])
{	
	void *lib = dlopen(argv[1], RTLD_NOW);

	if (lib == NULL) {
		puts(dlerror());
		return 1;
	}

	void (*f)(void) = dlsym(lib, "empty");

	char *error = dlerror();
	if (error != NULL) {
		fprintf(stderr, "%s\n", error);
		dlclose(lib);
		return 1;
	}

	f();

	dlclose(lib);

	return 0;
}
