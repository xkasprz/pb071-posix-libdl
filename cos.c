#include <stdio.h>
#include <dlfcn.h>

/** Calls cos(3) function from math library libm. 
 *  Uses fixed argument 1.0:).
 *
 *  It is here just to show, that libdl can be used with C-libraries.
 **/

int main(int argc, char *argv[])
{	
	/** You can give it full path or just name of .so
	 *  then it searches in $PATH **/

	// void *lib = dlopen("/lib64/libm.so.6", RTLD_NOW);
	
	void *lib = dlopen("libm.so.6", RTLD_NOW);

	if (lib == NULL) {
		puts(dlerror());
		return 1;
	}

	double (*kosinus) (double) = dlsym(lib, "cos");

	char *error = dlerror();
	if (error != NULL) {
		fprintf(stderr, "%s\n", error);
		dlclose(lib);
		return 1;
	}

	printf("%lf\n", kosinus(1.0));

	dlclose(lib);

	return 0;
}
