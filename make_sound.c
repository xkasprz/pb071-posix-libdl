#include <stdio.h>
#include <dlfcn.h>

/** Calls function make_sound from any given .so.
 *
 *  n. arg: .so with function char *(*make_sound)(void)
 *  -> none except what make_sound functions print
 *
 *  prepared .so:
 *  	cat.so
 *  	duck.so
 *  	rand.so -> prints random number (shows it that its not just 
 *  		like having file with that string)
 **/

int main(int argc, char *argv[])
{	
	if (argc != 2) {
		fprintf(stderr, "Expected one arg\n");
	}

	void *lib = dlopen(argv[1], RTLD_NOW);

	if (lib == NULL) {
		puts(dlerror());
		return 1;
	}

	char *(*make_sound)(void) = dlsym(lib, "make_sound");
		
	char *error = dlerror();
	if (error != NULL) {
		fprintf(stderr, "%s\n", error);
		dlclose(lib);
		return 1;
	}

	printf("%s\n", make_sound());

	dlclose(lib);

	return 0;
}
