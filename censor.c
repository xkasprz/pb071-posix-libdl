#include <ctype.h>

char *transform(const char *from, char *to) // ./transform ./censor.so
{
	int i;
	char c = 'o';

	for (i = 0; from[i] != 0; i++) {
		to[i] = from[i] == c ? 'X' : from[i];
	}

	to[i] = '\0';

	return to;
}
