#include <ctype.h>

char *transform(const char *from, char *to) // ./transform ./upper.so
{
	int i;

	for (i = 0; from[i] != 0; i++) {
		to[i] = toupper(from[i]);
	}

	to[i] = '\0';

	return to;
}
