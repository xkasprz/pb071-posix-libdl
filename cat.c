#include <stdio.h>

char *make_sound() // ./make_sound ./cat.so
{
	return "Meow";
}

void meow() // ./call_func ./cat.so meow
{
	puts("Meow");
}

void purr() // ./call_func ./cat.so purr
{
	puts("Vrrrr");
}
