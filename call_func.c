#include <stdio.h>
#include <dlfcn.h>

/** 
 * 1. arg: .so
 * 2. arg: name of function from .so to be called
 * -> calls function from .so
 *
 *  note: function to be called is expected to be void (*) (void)
 *
 *  prepared .so:
 *  	cat.so -> meow(), purr()
 *  	duck.so -> quack()
 *  	rand.so -> any(), dice()
 **/

int main(int argc, char *argv[])
{	
	if (argc != 3) {
		fprintf(stderr, "Expected two args\n");
		return 1;
	}

	void *lib = dlopen(argv[1], RTLD_NOW);

	if (lib == NULL) {
		puts(dlerror());
		return 1;
	}

	void(*func)(void) = dlsym(lib, argv[2]);
		
	char *error = dlerror();
	if (error != NULL) {
		fprintf(stderr, "%s\n", error);
		dlclose(lib);
		return 1;
	}

	func();

	dlclose(lib);

	return 0;
}
