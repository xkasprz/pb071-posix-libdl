# Jednoduchá ukázka použití libdl

man dlopen(3)
man dlsym(3)
man dlclose(3)
man dlerror(3)

Soubory se zkompilují make (all) + make clean mám zatím nabastlené na smazání meziproduktů.

Můžete si to zkusit spustit make test, ale výpisy si musíte zkontrolovat sami.

Ještě to trošku dobastlím, protože to možná pošlu svým studentům. (Kdyby jste to chtěli sdílet, tak mi prosím napište a já dám vědět, až to bude ve zveřejnitelné podobě:) [easter egg pro studenty co se k tomuto prokliknou]

--------------------------------------------------------------------------------
# TODO
makefile
-ldl (zdá se že není potřeba ale asi bych ho tam měla raději přihodit)
dokumentace

--------------------------------------------------------------------------------
Usage described in *.c

make_sound
    ./cat.so
    ./duck.so
    ./rand.so

call_func
    ./cat.so meow
    ./cat.so purr
    ./duck.so quack
    ./rand.so dice
    ./rand.so any

cos
    libm (hardcoded:)

transform
    ./upper.so
    ./lower.so
    ./censor.so
    ./error.so

void
    ./void-call

--------------------------------------------------------------------------------
